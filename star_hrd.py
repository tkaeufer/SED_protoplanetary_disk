import numpy as np

def load_all_star_lims():
    data_path='./star_data'
    lim_o_T=np.load(f'{data_path}/lim_o_T.npy')
    lim_o_L=np.load(f'{data_path}/lim_o_L.npy')
    T_low=np.load(f'{data_path}/T_low.npy')
    L_low=np.load(f'{data_path}/L_low.npy')
    T_up_mod=np.load(f'{data_path}/T_up_mod.npy')
    L_up_mod=np.load(f'{data_path}/L_up_mod.npy')
    lim_y_T=np.load(f'{data_path}/lim_y_T.npy')
    lim_y_L=np.load(f'{data_path}/lim_y_L.npy')
    return lim_o_T,lim_o_L,T_low,L_low,T_up_mod,L_up_mod,lim_y_T,lim_y_L

lim_o_T,lim_o_L,T_low,L_low,T_up_mod,L_up_mod,lim_y_T,lim_y_L=load_all_star_lims()

def normalizing(ax,ay,point):
    minx=np.min(ax)
    ptpx=np.ptp(ax)
    ax=(ax-minx)/ptpx
    
    miny=np.min(ay)
    ptpy=np.ptp(ay)
    ay=(ay-miny)/ptpy
    
    px=(point[0]-minx)/ptpx
    py=(point[1]-miny)/ptpy
    point=(px,py)
    return(ax,ay,point)

def find_nearest(arrayx,arrayy, point): #point in log
    ax = np.asarray(np.log10(arrayx))
    ay= np.asarray(np.log10(arrayy))
    ax,ay,point=normalizing(ax,ay,point)
    dist=[((point[0]-ax[i])**2+(point[1]-ay[i])**2) for i in range(len(ax))]
    idx=np.argmin(dist)
    return idx,arrayx[idx],arrayy[idx]

def in_or_out(arrayx,arrayy,point):
    idx,p1,p2=find_nearest(arrayx,arrayy,point)
    if arrayx[0]==lim_y_T[0] or arrayx[0]==lim_o_T[0]:
        try:
            v1=(np.log10(arrayx[idx+1])-np.log10(arrayx[idx]),np.log10(arrayy[idx+1])-np.log10(arrayy[idx]))
            v2=(np.log10(arrayx[idx+1])-point[0],np.log10(arrayy[idx+1])-point[1])
            xp = v1[0]*v2[1] - v1[1]*v2[0]  # Cross product
            xp2=+1
        except:
            xp=-1
            v1_2=(np.log10(arrayx[idx-1])-np.log10(arrayx[idx]),np.log10(arrayy[idx-1])-np.log10(arrayy[idx]))
            v2_2=(np.log10(arrayx[idx-1])-point[0],np.log10(arrayy[idx-1])-point[1])
            xp2 = v1_2[0]*v2_2[1] - v1_2[1]*v2_2[0]  # Cross product
    else:
        v1=(np.log10(arrayx[idx+1])-np.log10(arrayx[idx]),np.log10(arrayy[idx+1])-np.log10(arrayy[idx]))
        v2=(np.log10(arrayx[idx+1])-point[0],np.log10(arrayy[idx+1])-point[1])
        xp = v1[0]*v2[1] - v1[1]*v2[0]  # Cross product

        v1_2=(np.log10(arrayx[idx-1])-np.log10(arrayx[idx]),np.log10(arrayy[idx-1])-np.log10(arrayy[idx]))
        v2_2=(np.log10(arrayx[idx-1])-point[0],np.log10(arrayy[idx-1])-point[1])
        xp2 = v1_2[0]*v2_2[1] - v1_2[1]*v2_2[0]  # Cross product
    
    #for visualization:
    #xp2=1
    if xp > 0 or xp2<0:
        return True
    else:
        return False
    
def check_if_in(p0,p1):
    in_hrd=False
    if not in_or_out(lim_o_T,lim_o_L,(p0,p1)):
        if not in_or_out(T_low,L_low,(p0,p1)):
            if in_or_out(T_up_mod,L_up_mod,(p0,p1)):
                if in_or_out(lim_y_T,lim_y_L,(p0,p1)):
                    in_hrd=True
    return in_hrd