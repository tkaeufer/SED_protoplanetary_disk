import numpy as np

def load_sed(folder_name):
    seds=[]
    txt=str()
    with open(f'{folder_name}/wavelength.out','r') as f:
        lines=f.readlines()
    for line in lines[1:]:

        txt=txt+line.strip()+' '  
    txt=txt[1:-2].split()
    wavelength=np.array(txt,'float64')

    idx_sed=[]
    with open(f'{folder_name}/ClearedSED.out','r') as f:
    #with open(f'{folder_name}/MasterSED.out','r') as f:
        lines=f.readlines()
        for line in lines[1:]:
            pos=line.find('[')
            #print(pos)
            if pos!=-1:
                try:
                    SED=np.array(list(line[pos+1:-3].split()),'float64')
                    idx_sed.append([int(float(line[:pos-3])),int(line[pos-2:pos-1])])
                    seds.append(SED)
                except:
                    print('Error')
    idx_sed=np.asarray(idx_sed)
    seds=np.asarray(seds)
    print('Shape of SED, Wavlength and idx')
    print(np.shape(seds),np.shape(wavelength),len(idx_sed))
    return seds,wavelength,idx_sed

def load_para_input(folder_name):
    header=np.load(f'{folder_name}/header.npy')
    header=np.concatenate((header,['incl']),axis=0)
    print('Shape of header:')
    print(np.shape(header))
    matched_para=np.load(f'{folder_name}/matched_parameters.npy')
    print('Shape of parameter file:')
    print(np.shape(matched_para))
    return header,matched_para


def delete_infs(array1,array2):
    list1=[]
    list2=[]
    print('Shape before:')
    print(np.shape(array1),np.shape(array2))

    for j in range(len(array1)-1,-1,-1):
        line1=array1[j]
        line2=array2[j]
        if np.max(line1)<100 and np.min(line1)>-30:
            list1.append(line1)
            list2.append(line2)
            #np.delete(array1,j,axis=0)
            #np.delete(array2,j,axis=0)
        if j%10000==0:
            print(j,end='\r',flush=True)
    list1=np.asarray(list1)
    list2=np.asarray(list2)
    print('Shape after:')
    print(np.shape(list1),np.shape(list2))
    return list1,list2

def delete_incl(array_para,array_sed,value):
    print('Shape before:')
    print(np.shape(array_para),np.shape(array_sed))
    del_list=np.where(array_para[:,-1]>=value)[0]
    print('Number of lines to delete:')
    print(len(del_list))    
    new_len=len(array_para)-len(del_list)
    new_para=np.zeros((new_len,np.shape(array_para)[1]))
    new_sed=np.zeros((new_len,np.shape(array_sed)[1]))
    t=0
    for i in range(len(array_para)):
        if i not in del_list:
            new_para[t]=array_para[i]
            new_sed[t]=array_sed[i]
            t+=1
    print(f'{t} must be equal to {new_len}')
    return new_para,new_sed